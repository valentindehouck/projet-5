# Projet 5 - Kanap #

## INSTALLATION ##

From the "back" folder of the project, run `npm install`. You 
can then run the server with `node server`. 
The server should run on `localhost` with default port `3000`. If the
server runs on another port for any reason, this is printed to the
console when the server starts, e.g. `Listening on port 3001`.

## LIVRABLES ##

- Un fichier ZIP contenant le code fonctionnel du site web, nommé P5_nom_code.zip
- Le plan de test d'acceptation au format PDF, nommé P5_nom_plan_test.pdf pour convrir l'ensemble des fonctionnalités 


## ARCHICHECTURE ##

- PAGE D'ACCEUIL
- PAGE PRODUIT
- PAGE PANIER
- PAGE CONFIRMATION

## DESCRIPTIF PAGES ##

### PAGE D'ACCEUIL ###
Cette page présente l’ensemble des produits retournés par l’API.
Pour chaque produit, il faudra afficher l’image de celui-ci, ainsi que son nom et le début de
sa description.
En cliquant sur le produit, l’utilisateur sera redirigé sur la page du produit pour consulter
celui-ci plus en détail.

### PAGE PRODUIT ###
Cette page présente un seul produit ; elle aura un menu déroulant permettant à l'utilisateur
de choisir une option de personnalisation, ainsi qu’un input pour saisir la quantité. Ces
éléments doivent être pris en compte dans le panier.

### PAGE PANIER ###
Sur cette page, l’utilisateur va pouvoir modifier la quantité d’un produit de son panier ; à ce
moment, le total du panier devra bien se mettre à jour.
L’utilisateur aura aussi la possibilité de supprimer un produit de son panier, le produit devra
donc disparaître de la page.
Les inputs des utilisateurs doivent être analysés et validés pour vérifier le format et le type
de données avant l’envoi à l’API. Il ne serait par exemple pas recevable d’accepter un
prénom contenant des chiffres, ou une adresse e-mail ne contenant pas de symbole “@”. En
cas de problème de saisie, un message d’erreur devra être affiché en dessous du champ
correspondant.
Attention à ne pas stocker le prix des articles en local. Les données stockées en local ne
sont pas sécurisées et l’utilisateur pourrait alors modifier le prix lui-même.

### PAGE CONFIRMATION ###
Sur cette page, l'utilisateur doit voir s’afficher son numéro de commande. Il faudra veiller à
ce que ce numéro ne soit stocké nulle part.

### CODE SOURCE ###
Celui-ci devra être indenté et utiliser des commentaires en début de chaque fonction pour
décrire son rôle. Il devra également être découpé en plusieurs fonctions réutilisables
(nommées). Une fonction doit être courte et répondre à un besoin précis. Il ne faudrait pas
avoir de longues fonctions qui viendraient répondre à plusieurs besoins à la fois. Exemple : il
ne serait pas accepté de mettre une seule et unique fonction en place pour collecter, traiter
et envoyer des données.

### API ###
Concernant l’API, des promesses devront être utilisées pour éviter les callbacks. Il est
possible d’utiliser des solutions alternatives, comme fetch, celle-ci englobant la promesse.
L’API n’est actuellement que dans sa première version. La requête post qu’il faudra formuler
pour passer une commande ne prend pas encore en considération la quantité ni la couleur
des produits achetés.


## FONCTIONNEMENT DU PANIER ##
Dans le panier, les produits doivent toujours apparaître de manière regroupée par modèle et
par couleur.
Si un produit est ajouté dans le panier à plusieurs reprises, avec la même couleur, celui-ci
ne doit apparaître qu’une seule fois, mais avec le nombre d’exemplaires ajusté.
Si un produit est ajouté dans le panier à plusieurs reprises, mais avec des couleurs
différentes, il doit apparaître en deux lignes distinctes avec la couleur et la quantité
correspondantes indiquées à chaque fois.

## TYPES DE DONNÉES ##

Tous les produits possèdent les attributs suivants :

Champ       | Type
------------| ---------------
colors      | array of string
id          | string
name        | string
price       | number
imageUrl    | string
description | string
altTxt      | string


## URL DE L'API ##
● Catalogue de canapés : http://localhost:3000/api/products


## PARAMÈTRES DE L'API ##
Chaque API contient 3 paramètres :

Verbe | Paramètre | Corps de la demande prévue | Réponse
------|-----------|----------------------------|---------
GET   | /         |                            | Retourne un tableau de tous les éléments
GET   | /{product-ID} |                        | Renvoie l'élément correspondant
POST  | /order    | Requête JSON contenant un objet de contact et un tableau de produits | Retourne l'objet contact, le tableau produits et orderId (string)


## VALIDATION DES DONNÉES ##
Pour les routes POST, l’objet contact envoyé au serveur doit contenir les champs firstName,
lastName, address, city et email. Le tableau des produits envoyé au back-end doit être un
array de strings product-ID. Les types de ces champs et leur présence doivent être validés
avant l’envoi des données au serveur.